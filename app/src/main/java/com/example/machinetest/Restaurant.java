package com.example.machinetest;

import java.io.Serializable;

public class Restaurant implements Serializable {

    private String EnablePickUpOrdering;

    private String ChatFlag;

    private String Cuisines;

    private String RestaurantId;

    private String CityName;

    private String RestaurantLogo;

    private String RestaurantName;

    private String Distance;

    private String LocationName;

    public String getEnablePickUpOrdering() {
        return EnablePickUpOrdering;
    }

    public String getChatFlag() {
        return ChatFlag;
    }

    public String getCuisines() {
        return Cuisines;
    }

    public String getRestaurantId() {
        return RestaurantId;
    }

    public String getCityName() {
        return CityName;
    }

    public String getRestaurantLogo() {
        return RestaurantLogo;
    }

    public String getRestaurantName() {
        return RestaurantName;
    }

    public String getDistance() {
        return Distance;
    }

    public String getLocationName() {
        return LocationName;
    }

    public String getEnableOnlineOrdering() {
        return EnableOnlineOrdering;
    }

    private String EnableOnlineOrdering;

}
