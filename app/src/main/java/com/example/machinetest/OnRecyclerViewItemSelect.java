package com.example.machinetest;

public interface OnRecyclerViewItemSelect {
    void onSelect(int position);
}
