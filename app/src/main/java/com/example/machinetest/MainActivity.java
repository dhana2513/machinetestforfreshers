package com.example.machinetest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private String url = "https://fmprod.dishco.com/shawmanservices/api/RestaurantList/GetFunPubRestaurantList?IntMaxRecordCount=0&StrLocLocation=&StrLocCity=Mumbai&IntLocClubId=1&StrLocRestaurantName=&StrLocLatitude=18.9312631&StrLocLongitude=72.823837";
    private ArrayList<Restaurant> mListRestaurants;
    private RecyclerViewAdapter mRecyclerViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        setListeners();
        callApi();
    }

    private void init() {
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mListRestaurants = new ArrayList<>();
        mRecyclerViewAdapter = new RecyclerViewAdapter(mListRestaurants);
        recyclerView.setAdapter(mRecyclerViewAdapter);
    }

    private void setListeners() {
        mRecyclerViewAdapter.setOnRecyclerViewItemSelectListener(new OnRecyclerViewItemSelect() {
            @Override
            public void onSelect(int position) {
                RestaurentDetailsFrag frag = new RestaurentDetailsFrag();
                Bundle bundle = new Bundle();
                bundle.putSerializable(RestaurentDetailsFrag.KEY_RESTAURENT_DETAILS, mListRestaurants.get(position));
                frag.setArguments(bundle);

                getSupportFragmentManager().beginTransaction()
                        .addToBackStack(null)
                        .add(R.id.mainFrameLayout, frag).commit();
            }
        });
    }

    private void callApi() {

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                Type type = new TypeToken<ArrayList<Restaurant>>() {
                }.getType();
                ArrayList<Restaurant> listRestaurants = gson.fromJson(response, type);
                mListRestaurants.addAll(listRestaurants);
                mRecyclerViewAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> hasMap = new HashMap<>();
                hasMap.put("AndroidPhone", "EV6FTlgBhOalM+qjJpr2OZpAEpPkYJHC5I1aOWyeLevwSIpuzyKEAg==");
                return hasMap;
            }
        };

        RequestQueue requestQueu = Volley.newRequestQueue(this);
        requestQueu.add(request);
    }
}
