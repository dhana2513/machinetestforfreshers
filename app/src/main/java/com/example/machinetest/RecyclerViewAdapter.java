package com.example.machinetest;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private ArrayList<Restaurant> mListRestaurants;
    private String imageUrl = "https://foodmarshal.blob.core.windows.net/fmstorage/";
    private OnRecyclerViewItemSelect mOnRecyclerViewItemSelect;

    RecyclerViewAdapter(ArrayList<Restaurant> mListRestaurants) {
        this.mListRestaurants = mListRestaurants;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_resto_list, null);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(1, 1, 1, 1);
        view.setLayoutParams(params);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.txtName.setText(mListRestaurants.get(i).getRestaurantName());
        Picasso.get().load(imageUrl + mListRestaurants.get(i).getRestaurantLogo()).into(viewHolder.imgLogo);
    }

    @Override
    public int getItemCount() {
        return mListRestaurants.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtName;
        ImageView imgLogo;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mOnRecyclerViewItemSelect.onSelect(getAdapterPosition());
                }
            });
            imgLogo = itemView.findViewById(R.id.imgLogo);
            txtName = itemView.findViewById(R.id.txtName);
        }
    }

    public void setOnRecyclerViewItemSelectListener(OnRecyclerViewItemSelect mOnRecyclerViewItemSelect) {
        this.mOnRecyclerViewItemSelect = mOnRecyclerViewItemSelect;
    }
}
