package com.example.machinetest;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class RestaurentDetailsFrag extends Fragment {

    private Restaurant mRestaurant;
    public static final String KEY_RESTAURENT_DETAILS = "KEY_RESTAURENT_DETAILS";
    private ImageView imgLogo;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.lay_restaunrent_details_frag, null);
        init(view);
        setData();
        setListeners();
        return view;
    }

    private void init(View view) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        imgLogo = view.findViewById(R.id.imgLogo);
    }

    private void setData() {
        mRestaurant = (Restaurant) getArguments().getSerializable(KEY_RESTAURENT_DETAILS);
        Picasso.get().load("https://foodmarshal.blob.core.windows.net/fmstorage/" + mRestaurant.getRestaurantLogo())
                .into(imgLogo);
    }

    private void setListeners() {

    }
}
